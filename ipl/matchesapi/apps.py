from django.apps import AppConfig


class MatchesapiConfig(AppConfig):
    name = 'matchesapi'
