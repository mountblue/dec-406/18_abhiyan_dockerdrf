'''
Name:load_csv_db.py
Description:Load the csv and insert the row into MYSQL database
Input: Space separated filenames with having matches or deliveries in their name
Output : Data Loaded to database using Django ORM queries.
author : Abhiyan Timilsina
'''

from django.core.management.base import BaseCommand, CommandError
from matchesapi.models import Deliveries, Matches
import csv
import re
import os

# Path to the csv directory
ROOT_CSV_PATH = './csv_files/'


class Command(BaseCommand):
    help = 'Load CSV data to the database'
    db_row = []
   
    def add_arguments(self, parser):
        parser.add_argument('file_names', nargs='+', type=str)

    def load_file(self, file_name):
        # Check for the type of file
        type_of_file = file_name.find('match')
        file_path = os.path.join(ROOT_CSV_PATH, file_name)
        db_row = []
        count = 0
        with open(file_path) as file_pointer:
            file_content = csv.DictReader(file_pointer)
            for data_row in file_content:
                count += 1;
                if count == 2000:
                    count = 0
                    Deliveries.objects.bulk_create(db_row)
                    db_row = []
                    print('ADDED 2000')
                if type_of_file == 0:
                    match = Matches(*[value for key, value in data_row.items()])
                    db_row.append(match)        
                else:
                    d = Deliveries() 
                    d.match_id = Matches.objects.get(pk = data_row['match_id']) 
                    d.inning = data_row['inning']
                    d.batting_team = data_row['batting_team']
                    d.bowling_team = data_row['bowling_team']
                    d.over = data_row['over']
                    d.ball = data_row['ball']
                    d.batsman = data_row['batsman']
                    d.non_striker = data_row['non_striker']
                    d.bowler = data_row['bowler']
                    d.is_super_over = data_row['is_super_over']
                    d.wide_runs = data_row['wide_runs']
                    d.bye_runs = data_row['bye_runs']
                    d.legbye_runs = data_row['legbye_runs']
                    d.noball_runs = data_row['noball_runs']
                    d.penalty_runs = data_row['penalty_runs']
                    d.batsman_runs = data_row['batsman_runs']
                    d.extra_runs = data_row['extra_runs']
                    d.total_runs = data_row['total_runs']
                    d.player_dismissed = data_row['player_dismissed']
                    d.dismissal_kind = data_row['dismissal_kind']
                    d.fielder = data_row['fielder']
                    db_row.append(d)
        if type_of_file == 0:
            print(type_of_file)
            Matches.objects.bulk_create(db_row)
        else:
            Deliveries.objects.bulk_create(db_row)        
	    
    def handle(self, *args, **options):
        for file_name in options['file_names']:
            self.load_file(file_name)
        print("HELLO")
        self.stdout.write('SUCESSFULLY LOADED')                 
