
from django.urls import path, include
from .views import DeliveriesDetail, DeliveriesList, MatchesDetail, MatchesList, UserDetail, UserList

urlpatterns = [
    path('matches/', MatchesList.as_view()),
    path('deliveries/', DeliveriesList.as_view()),
    path('matches/<int:pk>', MatchesDetail.as_view(),name='match-detail'),
    path('deliveries/<int:pk>', DeliveriesDetail.as_view()),
    path('users/', UserList.as_view()),
    path('users/<int:pk>/', UserDetail.as_view()),
]

urlpatterns += [
    path('api-auth/', include('rest_framework.urls')),
]
