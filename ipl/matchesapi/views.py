from .serializers import MatchesSerializer, DeliveriesSerializer, UserSerializer
from .models import Matches, Deliveries
from rest_framework import generics
from rest_framework.pagination import PageNumberPagination
from django.contrib.auth.models import User
from rest_framework import permissions


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 1000
    page_size_query_param = 'page_size'
    max_page_size = 10000


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 1000


class MatchesList(generics.ListCreateAPIView):
    queryset = Matches.objects.all()
    serializer_class = MatchesSerializer
    pagination_class = StandardResultsSetPagination
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class MatchesDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Matches.objects.all()
    serializer_class = MatchesSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class DeliveriesDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Deliveries.objects.all()
    serializer_class = DeliveriesSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class DeliveriesList(generics.ListCreateAPIView):
    queryset = Deliveries.objects.all()
    serializer_class = DeliveriesSerializer
    pagination_class = LargeResultsSetPagination
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer    
