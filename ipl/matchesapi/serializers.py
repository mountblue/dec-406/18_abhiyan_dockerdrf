from rest_framework import serializers
from .models import Matches, Deliveries
from django.contrib.auth.models import User


class MatchesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Matches
        fields = "__all__"


class DeliveriesSerializer(serializers.HyperlinkedModelSerializer):
    match_id = serializers.HyperlinkedRelatedField(many=False, view_name='match-detail', read_only=True, lookup_url_kwarg='pk')

    class Meta:
        model = Deliveries
        fields = ['match_id', 'inning', 'bowling_team', 'batting_team', 'bowler', 'total_runs', 'extra_runs']


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = "__all__"
